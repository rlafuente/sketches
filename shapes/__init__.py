def crosshair(x, y, length=10):
    _ctx.line(x, y - length / 2, x, y + length / 2, stroke="#ff9944", strokewidth=2)
    _ctx.line(x - length / 2, y, x + length / 2, y, stroke="#ff9944", strokewidth=2)

'''
TODO: 
isometric block
pantone swatch
nytimes text box
post-it note (mural)
caption
random blob

'''
