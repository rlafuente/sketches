def norm(val, min, max, newmin, newmax):
    if type(val) in (int, float):
        val = [val]
    result = [newmin + (x - min) * (newmax - newmin) / (max - min) for x in val]
    if len(result) == 1:
        return result[0]
    return result


from datetime import date, datetime, timedelta
from typing import Sequence


def daterange(start_of_range: date, end_of_range: date) -> Sequence[date]:
    # https://stackoverflow.com/a/59428951/122400

    if start_of_range <= end_of_range:
        return [
            start_of_range + timedelta(days=x)
            for x in range(0, (end_of_range - start_of_range).days + 1)
        ]
    return [
        start_of_range - timedelta(days=x)
        for x in range(0, (start_of_range - end_of_range).days + 1)
    ]
